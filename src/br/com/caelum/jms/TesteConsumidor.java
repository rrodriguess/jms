package br.com.caelum.jms;

import javax.jms.*;
import javax.naming.InitialContext;
import java.util.Scanner;

public class TesteConsumidor {

    public static void main(String[] args) throws Exception {

        InitialContext context = new InitialContext();

        ConnectionFactory factory = (ConnectionFactory) context.lookup("ConnectionFactory");
        Connection connection = factory.createConnection();
        connection.start();

        Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        Destination fila = (Destination) context.lookup("financeiro");
        MessageConsumer consumer = session.createConsumer(fila);

        Message message = consumer.receive(1000); //Execução para por 10 segundos, após isso senão houver msg o método develve null e continua execução.
        System.out.println("Recebendo msg: " + message);

        new Scanner(System.in).nextLine(); //Parar o programa para testar a conexão.

        connection.close();
        context.close();

    }
}
